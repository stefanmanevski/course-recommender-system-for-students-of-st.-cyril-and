#!/usr/bin/python
# -*- coding: utf-8 -*-

import os
import numpy as np
import cPickle as pkl
import json
import codecs
import sets

from datetime import datetime
from collections import defaultdict
from operator import itemgetter

from sklearn import tree

from flask import render_template, request, make_response, abort, jsonify

from CourseRecommender import app, connection, models

@app.route('/', methods = ['GET'])
def home():
    return render_template('index.html', year=datetime.now().year)

@app.route('/predict/<int:index>/<int:semester>', methods = ['GET'])
def predict(index, semester):
    if isinstance(index, int) and isinstance(semester, int):
        if not is_valid_semester(index, semester):
            abort(make_response(jsonify("Селектиравте погрешен семестар!"), 400))
        
        sc = get_student_courses(index, semester - 1)
        
        if sc.shape[0] == 0:
            abort(make_response(jsonify("Нема доволно информации за да Ви се даде предлог предмети за следниот семестар!"), 501))
        
        faculty_group = get_faculty_group(sc[0][3])
        
        if faculty_group is None:
            abort(make_response(jsonify("Нема доволно информации за да Ви се даде предлог предмети за следниот семестар!"), 501))
        
        model = models.get(faculty_group + "_semester-" + str(semester - 1))
        
        if model is None:
            abort(make_response(jsonify("Нема доволно информации за да Ви се даде предлог предмети за следниот семестар!"), 501))      
        
        X = remove_none_values(sc)
        
        probabilities = np.log(model.predict_proba(X))
        courses_prob = []
        
        for prob in enumerate(probabilities):
            prob = prob[1]
            courses_prob.append(zip(model.classes_, prob))
        
        courses_prob = np.concatenate(np.array(courses_prob), axis=0)
        sorted_courses_prob = sorted(courses_prob, key=itemgetter(1), reverse = True)
        
        sorted_courses_ids, sorted_prob_ids = get_courses_prob_ids(sorted_courses_prob)
        
        results = json.dumps(get_courses_info(sorted_courses_ids, sorted_prob_ids, sc[0][3]), ensure_ascii = False).encode("utf-8")
        
        return jsonify(results = results)
    else:
        abort(make_response(jsonify("Внесете го правилно вашиот индекс и следен семестар!"), 400))

def get_courses_prob_ids(sorted_courses_prob):
    sorted_courses = []
    sorted_prob = []
    
    for item in sorted_courses_prob:
        sorted_courses.append(item[0])
        sorted_prob.append(item[1])
    
    return np.array(sorted_courses), np.array(sorted_prob)

def remove_none_values(sc):
    filtered_sc = []
    
    for row in sc:
        list = []
        for feature in row:
            if feature is not None:
                list.append(feature)
            else:
                list.append(0)
        filtered_sc.append(list)
        
    return np.array(filtered_sc)

def get_courses_info(courses, probs, facultyID):
    cursor = connection.cursor()
    course_details = []
    json_obj = []
    
    for courseID in courses:
        sql_command = ("SELECT c.ID, c.Name as CourseName, c.Credits, c.Lessons, c.StudyCycleID, p.Name as ProgrammeName, p.Code, f.ID as FacultyID, f.Name as FacultyName, f.ShortName " +
                        "FROM Courses as c INNER JOIN ProgrammesCourses as pc ON c.ID = pc.CourseID INNER JOIN Programmes as p ON pc.ProgrammeID = p.ID INNER JOIN Faculty as f ON c.FacultyId = f.ID " + 
                        "WHERE c.ID = " + str(courseID))
        cursor.execute(sql_command)
        course_details.append(cursor.fetchall())
    cursor.close()
    
    set = sets.Set()
    
    for i, details in enumerate(course_details):
        if probs[i] != float("-inf") and details[0][0] not in set:
            set.add(details[0][0])
            obj = {
                'prob' : probs[i],
                'courseID' : details[0][0],
                'courseName' : details[0][1],
                'credits' : str(details[0][2]),
                'lessons' : details[0][3],
                'studyCycleID' : details[0][4],
                'programmeName' : details[0][5],
                'programmeCode' : details[0][6],
                'facultyID' : details[0][7],
                'facultyName' : details[0][8],
                'facultyCode' : details[0][9]
            }
            json_obj.append(obj)
    
    return json_obj

def get_student_courses(index, semester):
    cursor = connection.cursor()
    
    sql_command = ("SELECT StudentID, SumAverage, SumCredits, FacultyID, ProgrammeID, CourseID, TeacherID, CourseSumAverage, ProgrammeSemesterPopularCourseID, TakePopularCourse, " +
                     "PassedExamsSemesterProbability, ChangedProgramme, OverwriteCourse, TotalNumberOfApply, NumberOfApply, TeacherCourseProbability, DependenceCourseGradeAverage " +
                     " FROM ts.Dataset WHERE StudentID = " + str(index) + " AND Semester = " + str(semester) + " AND MandatoryOptional = 0")
    cursor.execute(sql_command)
    rows = cursor.fetchall()
    cursor.close()
    
    return np.array(rows)

def get_faculty_group(facultyID):
    if facultyID == 1 or facultyID == 2 or facultyID == 3 or facultyID == 4 or facultyID == 6 or facultyID == 11 or facultyID == 13 or facultyID == 15 or facultyID == 23 or facultyID == 24 or facultyID == 27:
        return "technical"
    elif facultyID == 5 or facultyID == 8 or facultyID == 9 or facultyID == 21 or facultyID == 22 or facultyID == 25 or facultyID == 28 or facultyID == 30 or facultyID == 31 or facultyID == 32:
        return "general"
    elif facultyID == 14 or facultyID == 17 or facultyID == 18 or facultyID == 33:
        return "art"
    elif facultyID == 16 or facultyID == 26 or facultyID == 29:
        return "agriculture"
    elif facultyID == 19: 
        return "sport"
    elif facultyID == 7 or facultyID == 10 or facultyID == 12 or facultyID == 20:
        return "medical"
    else:
        return None

def is_valid_semester(index, semester):
    cursor = connection.cursor()
    
    sql_command = ("select a.* from Students as s full outer join (select ProgrammeID, Semester, COUNT(CourseID) as num from ProgrammesCourses where MandatoryOptional = 0 " +
                    "group by ProgrammeID, Semester) as a on s.LastProgrammeId = a.ProgrammeID where s.ID = " + str(index) + " and a.Semester = " + str(semester))
    cursor.execute(sql_command)
    rows = cursor.fetchall()
    cursor.close()
    
    return np.array(rows).size > 0
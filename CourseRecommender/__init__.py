from flask import Flask
import pyodbc
import os
import cPickle as pkl

app = Flask(__name__)

connection = pyodbc.connect('DRIVER={SQL Server}; SERVER=STEFAN\MANE; DATABASE = UKIM_IKNOW; UID = STEFAN\Stefan; PWD = ;Trusted_Connection = yes;')
cursor = connection.cursor()
cursor.execute("USE UKIM_IKNOW")

models = dict()
for filename in os.listdir("CourseRecommender\\models\\"):
    s = int(filename.split(".")[0].split("-")[1])
    faculty_group = filename.split("_")[0]
    fn = faculty_group + "_semester-" + str(s)

    with open("CourseRecommender\\models\\" + filename, "rb") as fid:
        model = pkl.load(fid)
        models[fn] = model

import CourseRecommender.views
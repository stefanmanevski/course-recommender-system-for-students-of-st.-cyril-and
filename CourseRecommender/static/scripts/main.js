﻿$(document).ready(function () {
    $("#error").hide();
    content = $("#coursesContent");

    $("#predictBtn").click(function () {
        $("#error").hide();

        var index = $("#index").val();
        var semester = $("#semester").val();
        
        if (Math.floor(index) != index || !$.isNumeric(index)) {
            $("#error").html("<a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>Внесете го правилно вашиот индекс!");
            $("#error").show();
        }
        else if (!semester || semester == "" || semester == "Select semester") {
            $("#error").html("<a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>Одберете го следниот семестар!");
            $("#error").show();
        }
        else {
            $.ajax({
                url: "/predict/" + index + "/" + semester,
                method: "GET",
                success: function (response) {
                    $("#error").hide();
                    
                    courses = JSON.parse(response.results);

                    var html = "";
                    courses.forEach(function (course, i) {
                        var counter;

                        if (i % 2 == 0) {
                            html += "<div class='row'>";
                            counter = 0;
                        }
                        else counter = 1;

                        html += "<div class='col-md-6 course'>"
                                    + "<div class='title'>"
                                        + "<h3>" + course.courseName + "</h3>"
                                    + "</div><hr />"
                                    + "<div class='subtitle'>"
                                        + "<h5>Credits: " + course.credits + "</h5>"
                                        + "<h5>Lessons: " + course.lessons + "</h5>"
                                    + "</div>"
                                    + "<div class='content'>"
                                        + "<h4>" + course.programmeName + "(" + course.programmeCode + "), " + course.facultyName + "</h4>"
                                    + "</div></div>";
                        
                        if (counter == 1)
                            html += "</div>";
                    });
                    content.html(html);
                }, error: function (error) {
                    $("#error").html(error.responseJSON);
                    $("#error").show();

                    content.html("");
                }
            });
        }
    });
});